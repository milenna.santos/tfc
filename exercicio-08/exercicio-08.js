let Player1 = 'Legolas Verdefolha';
let Player2 = 'Jadis, A Feiticeira Branca';

let ArmaP1 = 'Arco e Flecha';
let ArmaP2 = 'Varinha de Inverno';

let ForcaP1 = 100;
let ForcaP2 = 98;

let dado = 15;

ForcaP1 = (ForcaP1*dado) + 20;
ForcaP2 = (ForcaP2*dado) + 17;

ForcaP1 = ForcaP1 - (0*100);
ForcaP2 = ForcaP2 - (1*100);

if(ForcaP1 < 1800 && ForcaP2 < 1800){
    console.log(`${Player1} perdeu ${ForcaP1} pontos de vida no ataque de ${Player2}.`);
    console.log(`${Player2} perdeu ${ForcaP2} pontos de vida no ataque de ${Player1}.`);
    console.log('Todos sobreviveram.');
}else{
    if(ForcaP1 >= 1800 && ForcaP2 >= 1800){
        console.log(`${Player1} perdeu ${ForcaP1} pontos de vida no ataque de ${Player2}.`);
        console.log(`${Player2} perdeu ${ForcaP2} pontos de vida no ataque de ${Player1}.`);
        console.log('Todos morreram.');
    }else{
        if(ForcaP1 < 1800 && ForcaP2 >= 1800){
            console.log(`${Player1} perdeu ${ForcaP1} pontos de vida no ataque de ${Player2}.`);
            console.log(`${Player2} perdeu ${ForcaP2} pontos de vida no ataque de ${Player1}.`);
            console.log(`${Player2} morreu.`);
        }else{
            console.log(`${Player1} perdeu ${ForcaP1} pontos de vida no ataque de ${Player2}.`);
            console.log(`${Player2} perdeu ${ForcaP2} pontos de vida no ataque de ${Player1}.`);
            console.log(`${Player1} morreu.`)
        }
    }
}
